from apps.group import models as group_models


def getUserRole(user, group_id):
    group = group_models.Group.objects.get(pk=group_id)
    role_in_group = group.get_user_role_in_group(user_id=user.id)
    if role_in_group:
        return group.get_user_role_in_group(user_id=user.id).role
