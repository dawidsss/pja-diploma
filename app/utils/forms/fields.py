from django import forms
from django.core.exceptions import ValidationError

from apps.account import models as account_models
from apps.technology import models as technology_models


class BaseModelObjectField(forms.CharField):
    object_model = None
    error_message = None

    result_object = None
    create_if_none = None
    multiple_objects = None

    def __init__(self, multiple=False, create_if_none=False, *args, **kwargs):
        self.multiple_objects = multiple
        self.create_if_none = create_if_none
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        if self.multiple_objects:
            if not value:
                return []
            return value.replace(' ', '').split(',')
        else:
            return value

    def get_result_object(self, value):
        result = self.object_model.objects.filter(username=value).first()
        if result:
            return result
        else:
            raise ValidationError(self.error_message.format(value))

    def validate(self, value):
        super().validate(value)
        if self.multiple_objects:
            self.result_object = list()
            for value_object in value:
                model_object = self.get_result_object(value_object)
                self.result_object.append(model_object)
        else:
            model_object = self.get_result_object(value)
            self.result_object = model_object

    def clean(self, value):
        super().clean(value)
        return self.result_object


class UsersField(BaseModelObjectField):
    object_model = account_models.AppUser
    error_message = 'Nie znaleziono użytkownika: {}'


class TechnologyField(BaseModelObjectField):
    object_model = technology_models.Technology
    error_message = 'Nie znaleziono technologii: {}'

    def get_result_object(self, value):
        if self.create_if_none:
            result = self.object_model.objects.get_or_create(name=value)[0]
        else:
            result = self.object_model.objects.filter(name=value).first()
        if result:
            return result
        else:
            raise ValidationError(self.error_message.format(value))
