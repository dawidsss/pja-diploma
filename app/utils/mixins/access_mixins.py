from django import http
from django.shortcuts import redirect


class UserInGroup:
    def dispatch(self, request, pk, *args, **kwargs):
        if not request.user.diploma_groups.filter(pk=pk):
            return redirect('group:public-group-site-detail', pk)
        return super().dispatch(request, *args, **kwargs)


class IsSupervisor:
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_supervisor:
            return super().dispatch(request, *args, **kwargs)
        else:
            return http.HttpResponse('Unauthorized', 401)
