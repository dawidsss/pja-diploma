"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
                  path('', include('apps.index.urls')),
                  path('account/', include('apps.account.urls')),
                  path('group/', include('apps.group.urls')),
                  path('post/', include('apps.post.urls')),
                  path('technology/', include('apps.technology.urls')),
                  path('announcements/', include('apps.announcements.urls')),
                  path('user/', include('apps.user.urls')),
                  path('autocomplete/', include('apps.autocomplete.urls')),
                  path('attachment/', include('apps.attachment.urls')),
                  path('admin/', admin.site.urls),
                  path('tinymce/', include('tinymce.urls')),
                  path('meet/', include('apps.meets_calendar.urls')),
                  path('git/', include('apps.git_service.urls'))
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL,
                                                                                           document_root=settings.MEDIA_ROOT)
