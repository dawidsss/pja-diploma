from django.views import generic

from . import models


class TechnologyDetailView(generic.DetailView):
    model = models.Technology
    context_object_name = 'technology'


class TechnologySearchView(generic.ListView):
    model = models.Technology
    context_object_name = 'technologies'

    def get_queryset(self):
        technology_name = self.request.GET.get('technology_name')
        if technology_name:
            return models.Technology.objects.filter(name=technology_name)
        else:
            return super().get_queryset()


class TechnologyCreateView(generic.CreateView):
    model = models.Technology
    fields = ['name']
