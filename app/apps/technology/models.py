from django.db import models
from django.urls import reverse

from apps.account import models as account_models


class Technology(models.Model):
    name = models.CharField(max_length=50, verbose_name='Nazwa', help_text='Nazwa technologii')
    user = models.ForeignKey(account_models.AppUser, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Dodał', help_text='Dodał', editable=False)

    def get_absolute_url(self):
        return reverse('technology:technology-detail', args=[self.id])

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Technologia'
        verbose_name_plural = 'Technologie'