from django.apps import AppConfig


class TechnologyConfig(AppConfig):
    name = 'apps.technology'
