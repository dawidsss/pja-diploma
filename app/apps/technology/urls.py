from django.contrib.auth.views import LogoutView
from django.urls import path, include

from . import views

app_name = 'technology'
urlpatterns = [
    path('<int:pk>/', views.TechnologyDetailView.as_view(), name='technology-detail'),
    path('search/', views.TechnologySearchView.as_view(), name='technology-search'),
    path('new/', views.TechnologyCreateView.as_view(), name='technology-create'),
]
