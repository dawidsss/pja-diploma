from bootstrap_datepicker_plus import DatePickerInput
from dal import autocomplete, forward
from dal_select2_tagging.widgets import TaggingSelect2
from django import forms
from tinymce.widgets import TinyMCE

from apps.account import models as user_models
from utils.forms import fields
from . import models


class GroupForm(forms.ModelForm):
    team_leader = forms.ModelChoiceField(
        queryset=user_models.AppUser.objects.all(),
        widget=autocomplete.ModelSelect2(url='autocomplete:user_autocomplete')
    )

    team_members = forms.ModelMultipleChoiceField(
        required=False,
        queryset=user_models.AppUser.objects.all(),
        widget=autocomplete.ModelSelect2Multiple('autocomplete:user_autocomplete', forward=(
            'team_leader', forward.Self(), forward.Const({'limit': 4, 'only_supervisor': False}, 'args')))
    )

    graduation_date = forms.DateField(
        label='Przewidywana data obrony',
        widget=DatePickerInput(format='%d/%m/%Y')
    )

    technology = fields.TechnologyField(create_if_none=True, label='Technologie', multiple=True, max_length=500,
                                        widget=TaggingSelect2('autocomplete:technology_autocomplete'))

    class Meta:
        model = models.Group
        fields = ['title', 'graduation_date']


class GroupBySupervisorForm(forms.ModelForm):
    technology = fields.TechnologyField(create_if_none=True, label='Technologie', multiple=True, max_length=500,
                                        widget=TaggingSelect2('autocomplete:technology_autocomplete'))

    graduation_date = forms.DateField(
        label='Przewidywana data obrony',
        widget=DatePickerInput(format='%d/%m/%Y')
    )

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial')
        instance = kwargs.get('instance')
        technologies = instance.technologies.all()
        initial['technology'] = technologies
        super(GroupBySupervisorForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        self.instance.technologies.clear()
        return super().save(commit)

    class Meta:
        model = models.Group
        fields = ['title', 'graduation_date', 'git_repository', 'project_management_board', 'graduation_date']


class GroupByLeaderForm(forms.ModelForm):
    technology = fields.TechnologyField(create_if_none=True, label='Technologie', multiple=True, max_length=500,
                                        widget=TaggingSelect2('autocomplete:technology_autocomplete'))

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial')
        instance = kwargs.get('instance')
        technologies = instance.technologies.all()
        initial['technology'] = technologies
        super(GroupByLeaderForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        self.instance.technologies.clear()
        return super().save(commit)

    class Meta:
        model = models.Group
        fields = ['title', 'git_repository', 'project_management_board', 'informations']


class GroupAddMember(forms.Form):
    member = forms.ModelChoiceField(
        label='',
        queryset=user_models.AppUser.objects.all(),
        widget=autocomplete.ModelSelect2(url='autocomplete:user_autocomplete')
    )


class GroupSiteForm(forms.ModelForm):
    class Meta:
        model = models.GroupSite
        fields = ['content']
        widgets = {
            'content': TinyMCE(attrs={'cols': 80, 'rows': 30})
        }


class GroupAppUserForm(forms.ModelForm):
    class Meta:
        model = models.GroupAppUser
        fields = ['role']
        labels = {
            'role': ''
        }


class MembersForm(forms.Form):
    team_members = forms.ModelMultipleChoiceField(
        required=False,
        queryset=user_models.AppUser.objects.all(),
        widget=autocomplete.ModelSelect2(url='autocomplete:user_autocomplete')
    )


def get_groupappuser_formset():
    formset = forms.modelformset_factory(model=models.GroupAppUser, fields=('role',), labels={'role': ''}, extra=0)
    return formset
