import uuid

from django.db import models
from django.urls import reverse
from django.utils import translation, timezone

import apps
from apps.account import models as auth_models
from apps.technology import models as technology_models


class Group(models.Model):
    title = models.CharField(max_length=50, verbose_name='Tytuł', help_text='Tytuł pracy')
    last_post = models.DateTimeField(default=timezone.now, verbose_name='Ostatni post', help_text='Ostatni post')
    members = models.ManyToManyField(auth_models.AppUser, through='GroupAppUser', related_name='diploma_groups',
                                     verbose_name='Członkowie grupy', help_text='Członkowie grupy')
    technologies = models.ManyToManyField(technology_models.Technology, related_name='GroupTechnology',
                                          verbose_name='Technologie', help_text='Technologie')
    graduation_date = models.DateField(help_text='Format (dd/mm/YYYY)', verbose_name='Data obrony')
    git_repository = models.URLField(max_length=200, blank=True, verbose_name='Repozytorium',
                                     help_text='Musi zawierać (https:// lub http://)')
    project_management_board = models.URLField(max_length=200, blank=True, verbose_name='Tablica zadań',
                                               help_text='Musi zawierać (https:// lub http://)')
    informations = models.TextField(verbose_name='Informacje', help_text='Informacje o projekcie')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    @property
    def last_meet_date(self):
        meets = self.get_meets()
        if meets:
            return meets[0].date

    class Meta:
        verbose_name = 'Grupa'
        verbose_name_plural = 'Grupy'

    def get_absolute_url(self):
        return reverse('group:group-detail', args=[self.id])

    def save(self, *args, **kwargs):
        if not self.pk:
            super(Group, self).save(*args, **kwargs)
            GroupSite(group=self, content='', is_public=True).save()
            GroupSite(group=self, content='', is_public=False).save()
        else:
            super(Group, self).save(*args, **kwargs)

    def get_user_role_in_group(self, user_id):
        return GroupAppUser.objects.filter(group_id=self.id, user_id=user_id).first()

    def get_supervisor(self):
        return GroupAppUser.objects.filter(group=self, role=RoleInGroup.SUPERVISOR).first().user

    def get_leader(self):
        return GroupAppUser.objects.filter(group=self, role=RoleInGroup.LEADER).first().user

    def get_members(self):
        users = list()
        for appusergroup in GroupAppUser.objects.filter(group=self, role=RoleInGroup.STUDENT):
            users.append(appusergroup.user)
        users.append(self.get_leader())
        return users

    def get_members_with_supervisor(self):
        users = list()
        for appusergroup in GroupAppUser.objects.filter(group=self):
            users.append(appusergroup.user)
        return users

    def get_meets(self):
        meets_to_check = apps.meets_calendar.models.Meet.objects.filter(meet_with=self,
                                                                        date__gte=timezone.now().today()).order_by(
            'date',
            'start_time')
        meets = list()
        for meet in meets_to_check:
            if meet.date == timezone.now().today().date():
                if meet.end_time > timezone.now().today().time():
                    meets.append(meet)
            else:
                meets.append(meet)
        return meets

    def __str__(self):
        return f'Grupa: {self.title}'


class RoleInGroup(models.IntegerChoices):
    STUDENT = 0, translation.gettext_lazy('Student')
    LEADER = 1, translation.gettext_lazy('Lider')
    SUPERVISOR = 2, translation.gettext_lazy('Promotor')
    TECHNIC_SUPERVISOR = 3, translation.gettext_lazy('Promotor Techniczny')


class GroupAppUser(models.Model):
    user = models.ForeignKey(auth_models.AppUser, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    role = models.IntegerField(choices=RoleInGroup.choices, default=RoleInGroup.STUDENT)
    accepted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.group} - {self.user} - {self.get_role_display()}'


class GroupSite(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    content = models.TextField()
    is_public = models.BooleanField(default=False)

    def get_absolute_url(self):
        if self.is_public:
            return reverse('group:public-group-site-detail', args=[self.group_id])
        else:
            return reverse('group:private-group-site-detail', args=[self.group_id])
