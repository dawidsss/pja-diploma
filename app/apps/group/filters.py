import django_filters
from dal import autocomplete

from . import models
from ..account.models import AppUser


class GroupFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(method='title_filter', label='Tytuł')
    member = django_filters.ModelChoiceFilter(label='Członek', queryset=AppUser.objects.all(),
                                              widget=autocomplete.ModelSelect2(url='autocomplete:user_autocomplete'),
                                              method='member_filter')
    technology = django_filters.CharFilter(method='technology_filter', label='Technologia')


    class Meta:
        model = models.Group
        fields = ['title']

    def title_filter(self, queryset, name, value):
        return queryset.filter(title__istartswith=value)

    def member_filter(self, queryset, name, value):
        return queryset.filter(groupappuser__user__username=value)

    def technology_filter(self, queryset, name, value):
        return queryset.filter(technologies__name__istartswith=value)
