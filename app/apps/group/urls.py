from django.urls import path

from . import views

app_name = 'group'
urlpatterns = [
    path('', views.GroupListView.as_view(), {'title': 'testTitle'}, name='group-list'),
    path('my-groups/', views.UserGroupsListView.as_view(), {'title': 'testTitle'}, name='user-group-list'),
    path('<int:pk>', views.GroupDetailView.as_view(), name='group-detail'),
    path('new/', views.GroupCreateView.as_view(), name='group-create'),
    path('<int:pk>/update', views.GroupUpdateView.as_view(), name='group-update'),
    path('<int:pk>/site/private/', views.PrivateGroupSiteDetailView.as_view(), name='private-group-site-detail'),
    path('<int:pk>/site/public/', views.PublicGroupSiteDetailView.as_view(), name='public-group-site-detail'),
    path('<int:pk>/site/update/<int:is_public>', views.GroupSiteUpdateView.as_view(), name='group-site-update'),
    path('<int:pk>/members', views.GroupMembersView.as_view(), name='group-members'),
    path('<int:pk>/accept', views.GroupAcceptMembershipView.as_view(), name='group-accept-membership'),
    path('<int:groupid>/members/update/<int:userid>/<int:action>', views.GroupMembersManagementView.as_view(), name='group-management'),
    path('<int:pk>/members/add', views.AddMemberToGroupView.as_view(), name='add-group-user'),
    path('<int:pk>/integrations', views.GroupIntegrations.as_view(), name='group-integrations')
]
