import django_tables2 as tables
from django.utils import timezone

from apps.group import models
from apps.index import tables as base_tables


class GroupTable(tables.Table):
    title = tables.TemplateColumn('<a href="{{ record.get_absolute_url }}">{{ record.title }}</a>', verbose_name='Tytuł')
    graduation_date = tables.Column(verbose_name='Data Obrony')
    last_post = tables.Column('Ostatni Post')
    # last_meet_date = tables.Column()

    class Meta(base_tables.BaseTableMeta):
        model = models.Group
        fields = ('title', 'graduation_date', 'last_post', 'get_last_meet_date')
        attrs = {'class': 'table table-striped'}
        row_attrs = {'data-href': lambda record: record.get_absolute_url}
        order_by = 'get_last_meet_date'

    def order_get_last_meet_date(self, queryset, is_descending):
        print(queryset.query)

        queryset = queryset.extra(
            select={
                'get_last_meet_date': f'SELECT CONCAT(meets_calendar_meet.date, \' \', meets_calendar_meet.start_time) FROM meets_calendar_meet WHERE meets_calendar_meet.meet_with_id = group_group.id AND meets_calendar_meet.date >= \'{timezone.now().date()}\' AND meets_calendar_meet.end_time > \'{timezone.now().time()}\' ORDER BY meets_calendar_meet.date, meets_calendar_meet.end_time LIMIT 1'
            }
        )

        print(queryset.query)

        order_desc = ''
        # print(queryset.query)
        if is_descending:
            order_desc = '-'

        # print(queryset)
        # print(queryset.values())
        queryset = queryset.extra(
            order_by=[f'{order_desc}get_last_meet_date']
        )

        # queryset.order_by('last_meet_date')
        return (queryset, True)


class AllGroupsTable(tables.Table):
    title = tables.TemplateColumn('<a href="{{ record.get_absolute_url }}">{{ record.title }}</a>', verbose_name='Tytuł')
    get_supervisor = tables.Column(verbose_name='Promotor')
    get_leader = tables.Column(verbose_name='Lider')

    class Meta(base_tables.BaseTableMeta):
        model = models.Group
        fields = ('title', 'get_supervisor', 'get_leader')
        attrs = {'class': 'table table-striped'}
        row_attrs = {'data-href': lambda record: record.get_absolute_url}

class GroupAcceptMembershipTable(tables.Table):
    accept = tables.TemplateColumn('{% url \'group:group-accept-membership\' record.id %}', verbose_name='Akceptuj grupę')

    class Meta(base_tables.BaseTableMeta):
        models = models.Group
        fields = ('title',)
