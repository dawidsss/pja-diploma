from django import http
from django.contrib.auth import mixins
from django.shortcuts import render, redirect
from django.views import generic
from django_filters.views import FilterView

from utils.mixins import access_mixins
from . import models, forms, tables, filters
from ..index import views as base_views
from ..notifications import utils
from ..post import models as post_models


class GroupDetailView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin, access_mixins.UserInGroup,
                      generic.DetailView):
    model = models.Group
    context_object_name = 'group'

    def get(self, request, *args, **kwargs):
        utils.visit_group(self.request.user, self.get_object())
        return super().get(request, args, kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['members'] = models.GroupAppUser.objects.filter(group_id=self.kwargs.get('pk'))
        context['posts'] = post_models.Post.objects.filter(group_id=self.kwargs.get('pk')).order_by('-post_date')[:10]
        return context

    def can_use_config(self):
        user_role = self.object.get_user_role_in_group(user_id=self.request.user.id).role
        if user_role == models.RoleInGroup.SUPERVISOR or user_role == models.RoleInGroup.LEADER:
            return True


class GroupListView(mixins.LoginRequiredMixin, FilterView, base_views.BaseTableView):
    model = models.Group
    table_class = tables.AllGroupsTable
    filterset_class = filters.GroupFilter
    context_object_name = 'groups'
    paginate_by = 10
    template_name = "group/group_list.html"


class UserGroupsListView(GroupListView):
    table_class = tables.GroupTable

    def get_queryset(self):
        return self.request.user.diploma_groups.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['groups_to_accept'] = self.get_groups_to_accept()
        return context

    def get_groups_to_accept(self):
        app_user_groups = models.GroupAppUser.objects.filter(user_id=self.request.user.id, accepted=False).all()
        groups = [app_user_group.group for app_user_group in app_user_groups]
        return groups


class GroupCreateView(mixins.LoginRequiredMixin, generic.CreateView):
    model = models.Group
    form_class = forms.GroupForm

    def form_valid(self, form):
        result = super().form_valid(form)
        # SUPERVISOR
        supervisor_group = models.GroupAppUser(user=self.request.user, group=form.instance,
                                               role=models.RoleInGroup.SUPERVISOR)
        supervisor_group.save()

        # LEADER
        team_leader = form.cleaned_data['team_leader']
        team_leader_group = models.GroupAppUser(user=team_leader, group=form.instance,
                                                role=models.RoleInGroup.LEADER)
        team_leader_group.save()

        # MEMBERS
        for team_member in form.cleaned_data['team_members']:
            form.instance.members.add(team_member)

        # TECHNOLOGIES
        for technology in form.cleaned_data['technology']:
            form.instance.technologies.add(technology)

        return result


class GroupUpdateView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin, generic.UpdateView):
    model = models.Group

    def get_form_class(self):
        user_group_role = self.object.get_user_role_in_group(user_id=self.request.user.id)
        if user_group_role.role == models.RoleInGroup.SUPERVISOR:
            return forms.GroupBySupervisorForm
        elif user_group_role.role == models.RoleInGroup.LEADER:
            return forms.GroupByLeaderForm

    def form_valid(self, form):
        result = super().form_valid(form)

        for technology in form.cleaned_data.get('technology', []):
            form.instance.technologies.add(technology)

        return result


class GroupAcceptMembershipView(mixins.LoginRequiredMixin, generic.View):

    def get_group_user(self, group_id, user_id):
        return models.GroupAppUser.objects.filter(group_id=group_id, user_id=user_id).first()

    def get(self, request, pk):
        group_user = self.get_group_user(pk, self.request.user)
        if group_user:
            group_user.accepted = True
            group_user.save()
            return redirect(group_user.group)
        else:
            return http.HttpResponseNotFound()


class PrivateGroupSiteDetailView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin,
                                 access_mixins.UserInGroup, generic.DetailView):
    model = models.GroupSite
    context_object_name = 'site'

    def get_object(self, *args, **kwargs):
        return models.GroupSite.objects.filter(group_id=self.kwargs.get('pk'), is_public=False).first()


class PublicGroupSiteDetailView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin, generic.DetailView):
    model = models.GroupSite
    context_object_name = 'site'

    def get_object(self, *args, **kwargs):
        return models.GroupSite.objects.filter(group_id=self.kwargs.get('pk'), is_public=True).first()


class GroupSiteUpdateView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin, access_mixins.UserInGroup,
                          generic.UpdateView):
    model = models.GroupSite
    form_class = forms.GroupSiteForm

    def get_object(self, *args, **kwargs):
        if self.kwargs.get('is_public'):
            return models.GroupSite.objects.filter(group_id=self.kwargs.get('pk'), is_public=True).first()
        else:
            return models.GroupSite.objects.filter(group_id=self.kwargs.get('pk'), is_public=False).first()


class GroupMembersView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin, access_mixins.IsSupervisor,
                       generic.View):

    def get_group(self, pk):
        return models.Group.objects.filter(pk=pk).first()

    def get_add_user_form(self):
        return forms.GroupAddMember

    def get_context_data(self, group, **kwargs):
        context = super().get_context_data()
        context['add_user_form'] = self.get_add_user_form()
        context['group'] = group
        context['leader'] = group.get_leader()
        return context

    def get(self, request, pk):
        group = self.get_group(pk)
        if not group:
            return http.HttpResponseNotFound('Group not found')
        context = self.get_context_data(group)
        return render(request, 'group/group_members_form.html', context=context)

    def post(self, request, pk):
        group = self.get_group(pk)
        formset = forms.get_groupappuser_formset()
        group_members_formset = formset(request.POST)
        if group_members_formset.is_valid():
            group_members_formset.save()
            return redirect(group)
        else:
            return http.HttpResponseBadRequest(group_members_formset.errors)


class AddMemberToGroupView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin, access_mixins.IsSupervisor,
                           generic.View):
    def post(self, request, pk):
        form = forms.GroupAddMember(request.POST)
        if form.is_valid():
            member = form.cleaned_data['member']
            if not models.GroupAppUser.objects.filter(user=member, group_id=pk):
                member_group = models.GroupAppUser(user=member, group_id=pk,
                                                   role=models.RoleInGroup.STUDENT)
                member_group.save()
        return redirect('group:group-members', pk)


class GroupMembersManagementView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin,
                                 access_mixins.IsSupervisor, generic.View):

    def get(self, request, groupid, userid, action):
        group = models.Group.objects.filter(pk=groupid).first()
        member = models.GroupAppUser.objects.filter(group_id=groupid, user_id=userid).first()
        if action == 1:  # Promote leader
            leaderid = group.get_leader().id
            leader = models.GroupAppUser.objects.filter(group_id=groupid, user_id=leaderid).first()
            leader.role = models.RoleInGroup.STUDENT
            leader.save()
            member.role = models.RoleInGroup.LEADER
            member.save()
        elif action == 2:  # Delete User
            member.delete()
        return redirect('group:group-members', pk=groupid)


class GroupIntegrations(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin,
                                 access_mixins.UserInGroup, generic.DetailView):
    model = models.Group
    context_object_name = 'group'
    template_name = 'group/group_integrations.html'
