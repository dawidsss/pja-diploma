# Generated by Django 3.1.2 on 2021-04-07 17:29

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('group', '0003_auto_20210313_1722'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='graduation_date',
            field=models.DateField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
