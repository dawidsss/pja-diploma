from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils import timezone

from apps.account import models as account_models
from apps.group import models as group_models
from apps.notifications import utils


class Post(models.Model):
    author = models.ForeignKey(account_models.AppUser, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Autor', help_text='Autor')
    group = models.ForeignKey(group_models.Group, on_delete=models.CASCADE, verbose_name='Grupa', help_text='Grupa')

    content = models.TextField(max_length=500, verbose_name='Treść', help_text='Treść')
    post_date = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Data', help_text='Data')

    integration = models.TextField(max_length=50, blank=True, choices=settings.INTEGRATIONS_CHOICES)

    def get_absolute_url(self):
        return reverse('group:group-detail', args=[self.group_id])

    def save(self, *args, **kwargs):
        if not self.pk:
            super().save(*args, **kwargs)
            self.group.last_post = timezone.now()
            self.group.save()
        else:
            super().save(*args, **kwargs)
        if self.integration:
            utils.add_notification('integracja', self.group, f'Nowy commit w grupie {self.group.title}')
        else:
            utils.add_notification(self.author, self.group, f'Nowy post w grupie {self.group.title}')

    def __str__(self):
        return f'id:{self.id}, grupa:{self.group.id}, autor:{self.author}'

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posty'