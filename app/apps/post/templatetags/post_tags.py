from django import template
from django.conf import settings

from apps.post import forms

register = template.Library()


@register.inclusion_tag('post/post_detail.html', name='render_post')
def render_post(post):
    integration = None
    if post.integration:
        integration = settings.INTEGRATIONS.get(post.integration, None)
    return {'post': post, 'integration': integration}

@register.inclusion_tag('post/post_form.html', name='post_form')
def post_form(group_id, user):
    form = forms.CreatePostForm()

    return {'form': form,
            'group_id': group_id,
            'user': user}
