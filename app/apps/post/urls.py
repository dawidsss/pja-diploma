from django.urls import path

from . import views

app_name = 'post'
urlpatterns = [
    path('<int:pk>/new/', views.CreatePostView.as_view(), name='create-post')
]
