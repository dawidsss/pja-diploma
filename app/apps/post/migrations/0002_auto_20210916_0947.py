# Generated by Django 3.1.2 on 2021-09-16 07:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('group', '0008_auto_20210916_0947'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('post', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='post',
            options={'verbose_name': 'Post', 'verbose_name_plural': 'Posty'},
        ),
        migrations.AlterField(
            model_name='post',
            name='author',
            field=models.ForeignKey(help_text='Autor', null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Autor'),
        ),
        migrations.AlterField(
            model_name='post',
            name='content',
            field=models.CharField(help_text='Treść', max_length=500, verbose_name='Treść'),
        ),
        migrations.AlterField(
            model_name='post',
            name='group',
            field=models.ForeignKey(help_text='Grupa', on_delete=django.db.models.deletion.CASCADE, to='group.group', verbose_name='Grupa'),
        ),
        migrations.AlterField(
            model_name='post',
            name='post_date',
            field=models.DateTimeField(auto_now_add=True, help_text='Data', verbose_name='Data'),
        ),
    ]
