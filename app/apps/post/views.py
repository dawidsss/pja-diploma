from django.contrib.auth import mixins
from django.views import generic

from utils.mixins import access_mixins
from . import models, forms


class CreatePostView(mixins.LoginRequiredMixin, access_mixins.UserInGroup, generic.CreateView):
    model = models.Post
    form_class = forms.CreatePostForm

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.group_id = self.kwargs.get('pk')
        return super(CreatePostView, self).form_valid(form)
