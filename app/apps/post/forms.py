from django import forms

from . import models


class CreatePostForm(forms.ModelForm):
    class Meta:
        model = models.Post
        fields = ['content']
        labels = {'content': ''}
        help_texts = {'content': ''}
        widgets = {'content': forms.Textarea(attrs={'rows': 2, 'cols':50})}

