from django.urls import path

from . import views

app_name = 'announcement'
urlpatterns = [
    path('', views.AnnouncementListView.as_view(), name='announcement-list'),
    # path('my-groups/', views.UserGroupsListView.as_view(), name='user-group-list'),
    # path('<int:pk>', views.GroupDetailView.as_view(), name='group-detail'),
    path('new/', views.AnnouncementCreateView.as_view(), name='announcement-create'),
]
