# Generated by Django 3.1.2 on 2021-09-16 07:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('announcements', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='announcement',
            options={'verbose_name': 'Ogłoszenie', 'verbose_name_plural': 'Ogłoszenia'},
        ),
        migrations.AlterField(
            model_name='announcement',
            name='content',
            field=models.TextField(help_text='Treść', verbose_name='Treść'),
        ),
        migrations.AlterField(
            model_name='announcement',
            name='date',
            field=models.DateTimeField(auto_now=True, help_text='Data', verbose_name='Data'),
        ),
        migrations.AlterField(
            model_name='announcement',
            name='user',
            field=models.ForeignKey(help_text='Użytkownik', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Użytkownik'),
        ),
    ]
