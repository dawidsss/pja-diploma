from django.db import models
from django.urls import reverse

from apps.account.models import AppUser


class Announcement(models.Model):
    content = models.TextField(verbose_name='Treść', help_text='Treść')
    date = models.DateTimeField(auto_now=True, verbose_name='Data', help_text='Data')
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, verbose_name='Użytkownik', help_text='Użytkownik')

    def get_absolute_url(self):
        return reverse('announcement:announcement-list')

    class Meta:
        verbose_name = 'Ogłoszenie'
        verbose_name_plural = 'Ogłoszenia'