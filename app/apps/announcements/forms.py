from django import forms
from tinymce.widgets import TinyMCE

from . import models


class AnnouncementForm(forms.ModelForm):
    class Meta:
        model = models.Announcement
        fields = ['content']
        help_texts = {'content': ''}
        widgets = {'content': TinyMCE(attrs={'cols': 80, 'rows': 2})}

