from django.contrib.auth import mixins
from django.views import generic

from apps.announcements import models, forms


class AnnouncementCreateView(mixins.LoginRequiredMixin, generic.CreateView):
    model = models.Announcement
    form_class = forms.AnnouncementForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(AnnouncementCreateView, self).form_valid(form)


class AnnouncementListView(mixins.LoginRequiredMixin, generic.ListView):
    model = models.Announcement
    context_object_name = 'announcements'
    paginate_by = 10
