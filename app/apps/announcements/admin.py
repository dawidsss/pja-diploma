from django.contrib import admin

from apps.announcements import models

admin.site.register(models.Announcement)

