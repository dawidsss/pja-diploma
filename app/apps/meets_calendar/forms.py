from bootstrap_datepicker_plus import DatePickerInput, TimePickerInput
from django import forms

from . import models


class NewMeetForm(forms.ModelForm):

    class Meta:
        model = models.Meet
        fields = ['date', 'start_time', 'end_time', 'description']
        widgets = {
            'date': DatePickerInput(format='%d/%m/%Y'),
            'start_time': TimePickerInput(),
            'end_time': TimePickerInput()
        }
