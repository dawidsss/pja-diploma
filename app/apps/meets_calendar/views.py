from django.contrib.auth import mixins
from django.urls import reverse
from django.views import generic

from . import models, forms
from utils.mixins import access_mixins


class CreateNewMeetingView(mixins.LoginRequiredMixin, access_mixins.IsSupervisor, generic.CreateView):
    model = models.Meet
    form_class = forms.NewMeetForm
    template_name = 'meets_calendar/meetModalForm.html'

    def form_valid(self, form):
        form.instance.meet_with_id = self.kwargs.get('group_id')
        form.instance.user_id = self.request.user.id
        return super(CreateNewMeetingView, self).form_valid(form)

    def get_success_url(self):
        return reverse('group:group-detail', kwargs={'pk': self.kwargs.get('group_id')})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        context['group_id'] = self.kwargs.get('group_id')
        print(context)
        return context