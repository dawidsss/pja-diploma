from django.apps import AppConfig


class MeetsCalendarConfig(AppConfig):
    name = 'meets_calendar'
