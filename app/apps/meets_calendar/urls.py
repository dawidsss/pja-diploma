from django.urls import path

from . import views

app_name = 'meet'
urlpatterns = [
    path('<int:group_id>/new', views.CreateNewMeetingView.as_view(), name='new-meet')
]
