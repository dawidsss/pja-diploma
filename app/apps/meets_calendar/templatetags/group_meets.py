from django import template
from django.utils import timezone

from apps.meets_calendar import models

register = template.Library()


@register.inclusion_tag('meets_calendar/meets.html', name='render_group_meets')
def render_group_meets(group, user):
    meets = group.get_meets()
    return {'meets': meets, 'group': group, 'user': user}
