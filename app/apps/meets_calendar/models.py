from django.db import models

from apps.account import models as auth_models
from apps.group import models as group_models


class Meet(models.Model):
    user = models.ForeignKey(auth_models.AppUser, on_delete=models.CASCADE, verbose_name='Użytkownik')
    date = models.DateField(verbose_name='Data')
    start_time = models.TimeField(verbose_name='Start')
    end_time = models.TimeField(verbose_name='Koniec')
    meet_with = models.ForeignKey(group_models.Group, on_delete=models.CASCADE, verbose_name='Spotkanie z')
    description = models.CharField(max_length=500, verbose_name='Opis')
