from apps.notifications.models import Notification


def visit_group(user, group):
    notifications = Notification.objects.filter(user=user, group=group)
    notifications.delete()


def add_notification(sender, group, message):
    for user in group.get_members_with_supervisor():
        if user is not sender:
            notification = Notification(user=user, group=group, content=message)
            notification.save()
