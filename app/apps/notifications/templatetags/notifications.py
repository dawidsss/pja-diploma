from django import template

from apps.notifications import models

register = template.Library()

@register.inclusion_tag('notification/notifications.html', name='notifications')
def render_post(user):
    notifications = models.Notification.objects.filter(user=user)
    return {'notifications': notifications}
