from django.db import models

from apps.account.models import AppUser
from apps.group.models import Group


class Notification(models.Model):
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, verbose_name='Użytkownik', help_text='Użytkownik')
    content = models.TextField(max_length=200, verbose_name='Wiadomość', help_text='Wiadomość')
    group = models.ForeignKey(Group, on_delete=models.CASCADE, verbose_name='Grupa', help_text='Grupa')
