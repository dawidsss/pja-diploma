from django.contrib.auth import models as auth_models
from django.db import models
from django.templatetags.static import static
from django.urls import reverse


def avatar_directory(instance, filename):
    return f'avatars/{instance.id}/{filename}'


class AppUser(auth_models.AbstractUser):
    is_supervisor = models.BooleanField(default=False, verbose_name='Czy promotor', help_text='Czy promotor')
    email = models.EmailField('Email address', unique=True, help_text='Adres e-mail')
    student_nr = models.CharField(max_length=32, unique=True, verbose_name='Nr studenta', help_text='Nr studenta')
    description = models.TextField(max_length=500, blank=True, verbose_name='Opis', help_text='Opis')
    avatar = models.ImageField(upload_to=avatar_directory, blank=True, verbose_name='Zdjęcie Profilowe')

    def get_avatar_url(self):
        if self.avatar:
            return self.avatar.url
        else:
            return static('base/img/default_avatar.jpg')

    def save(self, *args, **kwargs):
        if not self.student_nr:
            self.student_nr = self.email.split('@')[0]
        super(AppUser, self).save(*args, **kwargs)

    def __str__(self):
        return self.username

    def get_success_url(self):
        return reverse('group:user-group-list')

    class Meta:
        verbose_name = 'Użytkownik'
        verbose_name_plural = 'Użytkownicy'
