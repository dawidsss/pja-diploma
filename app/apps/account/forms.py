from django import forms
from tinymce.widgets import TinyMCE

from . import models


class AppUserForm(forms.ModelForm):
    class Meta:
        model = models.AppUser
        fields = ['avatar', 'description']
        widgets = {'description': TinyMCE(attrs={'cols': 80, 'rows': 2})}
