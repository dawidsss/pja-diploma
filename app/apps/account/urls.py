from django.contrib.auth.views import LogoutView
from django.urls import path, include

from . import views

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('social/', include('social_django.urls', namespace='social'), name='index'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('settings/', views.UserUpdateView.as_view(), name='user-update'),
]
