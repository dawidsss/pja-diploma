from django import shortcuts
from django.contrib import auth
from django.contrib.auth import mixins, authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render
from django.urls import reverse
from django.views import generic, View
from . import models
from . import forms




class UserUpdateView(mixins.LoginRequiredMixin, generic.UpdateView):
    model = models.AppUser
    form_class = forms.AppUserForm

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('group:user-group-list')


class LoginView(View):
    def get(self, request):
        context = {'form': AuthenticationForm()}
        if request.GET.get('error', False):
            context['social_error'] = 'Wystąpił błąd. Aby zalogować się użyj maila z domeną pjwstk.edu.pl'
        return render(request, 'account/login.html', context=context)

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        context = {'form': AuthenticationForm()}

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return shortcuts.redirect('index')

        context['error_message'] = 'Nieprawidłowa nazwa użytkownika lub hasło'
        return render(request, 'account/login.html', context=context)
