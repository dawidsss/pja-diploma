from rest_framework import views, parsers
from rest_framework.response import Response

from apps.group import models as group_models
from apps.post import models as post_models


class GitBase(views.APIView):
    parser_classes = (parsers.JSONParser,)

    def post(self, request, uuid):
        group = self.get_group(uuid)

        if not group:
            return Response('Group not found', 404)

        try:
            self.process_response(group, request.data)
        except Exception as err:
            return Response({'message': 'Error parsing date', 'exception': err})

        return Response(f'Success')

    def get_group(self, uuid):
        return group_models.Group.objects.filter(uuid=uuid).first()

    def process_response(self, group, data):
        raise NotImplementedError()


class Bitbucket(GitBase):

    def process_response(self, group, data):
        commits = data.get('push').get('changes')[0].get('commits')

        for commit in commits:
            post = post_models.Post()
            post.group = group
            post.integration = 'Bitbucket'
            link = commit.get('links').get('html').get('href')
            author = commit.get("author").get('raw')

            post.content = f'Użytkownik {author} wprowadził nowe zmiany <a href="{link}" target="_blank">commit</a> <br />Wiadomość: {commit.get("message")}'
            post.save()
