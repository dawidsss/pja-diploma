from django.apps import AppConfig


class GitServiceConfig(AppConfig):
    name = 'apps.git_service'
