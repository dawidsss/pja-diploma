from django.urls import path

from apps.git_service import views

app_name = 'git_service'
urlpatterns = [
    path('bitbucket/<uuid:uuid>', views.Bitbucket.as_view(), name='bitbucket')
]