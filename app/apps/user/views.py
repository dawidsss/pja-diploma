from django.contrib import auth
from django.views import generic


class SupervisorsListView(generic.ListView):
    template_name = 'user/supervisors_list.html'
    context_object_name = 'supervisors'
    paginate_by = 10

    def get_queryset(self):
        user_model = auth.get_user_model()
        return user_model.objects.filter(is_supervisor=True)
