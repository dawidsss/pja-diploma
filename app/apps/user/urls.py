from django.urls import path

from . import views

app_name = 'user'
urlpatterns = [
    path('supervisors/', views.SupervisorsListView.as_view(), name='supervisor-list'),
]
