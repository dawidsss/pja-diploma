from django.urls import path

from . import views

app_name = 'attachment'
urlpatterns = [
    path('<int:pk>/new', views.CreateAttachmentView.as_view(), name='create-attachment')
]
