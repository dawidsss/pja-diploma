import datetime

from django.core.exceptions import ValidationError
from django.db import models
from apps.group import models as group_models


def group_directory_path(instance, filename):
    return f'attachments/{instance.group.id}/{datetime.datetime.now()}-{filename}'


def validate_file_size(value):
    filesize = value.size

    if filesize > 10485760:
        raise ValidationError('The maximum file size that can be uploaded is 10MB')
    else:
        return value


class Attachment(models.Model):
    name = models.CharField(max_length=100)
    group = models.ForeignKey(group_models.Group, on_delete=models.CASCADE)
    file = models.FileField(upload_to=group_directory_path, validators=[validate_file_size])
    create_date = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Data', help_text='Data')

    def __str__(self):
        return self.name
