from django.contrib.auth import mixins
from django.urls import reverse
from django.views import generic

from utils.mixins import access_mixins
from . import models, forms
from apps.index import views as base_views


class CreateAttachmentView(base_views.UserWithRoleInGroupMixin, mixins.LoginRequiredMixin, access_mixins.UserInGroup, generic.CreateView):
    model = models.Attachment
    form_class = forms.CreateAttachmentForm

    def get_success_url(self):
        return reverse('group:group-detail', kwargs={'pk': self.kwargs.get('pk')})

    def form_valid(self, form):
        form.instance.group_id = self.kwargs.get('pk')
        return super(CreateAttachmentView, self).form_valid(form)
