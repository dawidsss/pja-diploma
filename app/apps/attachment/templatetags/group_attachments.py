from django import template

from apps.attachment import models

register = template.Library()


@register.inclusion_tag('attachment/attachemnts.html', name='render_attachments')
def render_post(group):
    attachments = models.Attachment.objects.filter(group=group)
    return {'attachments': attachments}
