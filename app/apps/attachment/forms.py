from django import forms

from . import models


class CreateAttachmentForm(forms.ModelForm):
    class Meta:
        model = models.Attachment
        fields = ['name', 'file']
