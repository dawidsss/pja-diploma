import django_tables2 as tables
from django import http
from django import shortcuts
from django.shortcuts import redirect
from django.views import generic

from utils.users import userUtils


class IndexView(generic.View):

    def get(self, request: http.HttpRequest):
        if request.user.is_authenticated:
            return redirect('group:user-group-list')
        else:
            return redirect('login')


class AuthenticationErrorView(generic.TemplateView):
    template_name = 'authentication_error.html'


class UserWithRoleInGroupMixin:

    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
        except:
            context = {}
        pk = self.kwargs.get('pk')
        if pk:
            if not context.get('group'):
                context['group'] = {'id': pk}
            context['role'] = userUtils.getUserRole(self.request.user, pk)
        return context


class RedirectOnSingleResultMixin:

    def get(self, request, *args, **kwargs):
        result = super().get(request, *args, **kwargs)
        if len(self.object_list) == 1:
            result_object = self.object_list.first()
            return redirect(result_object.get_absolute_url())
        return result


class BaseListView(RedirectOnSingleResultMixin, generic.ListView):
    pass


class BaseTableView(RedirectOnSingleResultMixin, tables.SingleTableView):
    pass
