from django.urls import path

from apps.index import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('authentication_error/', views.AuthenticationErrorView.as_view(), name='authentication_error')
]
