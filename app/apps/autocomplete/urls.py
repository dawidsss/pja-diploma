from dal_select2.views import Select2QuerySetView
from django.urls import path

from apps.autocomplete import views
from apps.technology import models

app_name = 'autocomplete'
urlpatterns = [
    path('user/', views.UserAutocomplete.as_view(), name='user_autocomplete'),
    path('technology/', Select2QuerySetView.as_view(model=models.Technology, create_field='name'),
         name='technology_autocomplete')
]
