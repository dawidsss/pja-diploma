from dal import autocomplete
from django.db.models import Q

from apps.account import models as user_models
from apps.technology import models as technology_models


class UserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return user_models.AppUser.objects.none()

        if self.q:
            qs = user_models.AppUser.objects.all()

            team_leader = self.forwarded.get('team_leader', None)
            if team_leader:
                qs = qs.filter(~Q(id=team_leader))

            team_members = self.forwarded.get('self', [])
            if team_members:
                qs = qs.filter(~Q(id__in=team_members))

            args = self.forwarded.get('args', {})
            if args.get('limit'):
                if len(team_members) >= args.get('limit'):
                    qs = user_models.AppUser.objects.none()

            if args.get('only_supervisor'):
                qs = qs.filter(is_supervisor=True)

            qs = qs.filter(
                Q(first_name__istartswith=self.q) | Q(last_name__istartswith=self.q) | Q(username__istartswith=self.q))

            return qs[:5]
        else:
            return user_models.AppUser.objects.none()

    def get_result_label(self, result):
        return f'{result.username} ({result.first_name} {result.last_name})'


class TechnologyAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return technology_models.Technology.objects.none()

        if self.q:
            qs = technology_models.Technology.objects.all()

            qs = qs.filter(name__istartswith=self.q)

            return qs[:5]

        else:
            return technology_models.Technology.objects.none()

    def get_result_label(self, result):
        return f'{result.username} ({result.first_name} {result.last_name})'
